package com.client.base.uitest.admin;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class GridDemo {
	
	WebDriver driver;
    String baseURL, nodeURL;

    @BeforeTest
    public void setUp() throws MalformedURLException {
        baseURL = "https://opensource-demo.orangehrmlive.com";
        nodeURL = "http://192.168.10.6:4444/wd/hub/";
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        capability.setBrowserName("chrome");
        driver = new RemoteWebDriver(new URL(nodeURL), capability);
        driver.manage().window().maximize();
    }


    @Test
    public void sampleTest() {
        driver.get(baseURL);
        System.out.println("Application launched successfully");

    }
    
//    @Test
//    public void sampleTest1() {
//        driver.get(baseURL);
//        System.out.println("Application launched successfully");
//
//    }
    
    @AfterTest
    public void afterTest() {
        driver.quit();
    }

}
