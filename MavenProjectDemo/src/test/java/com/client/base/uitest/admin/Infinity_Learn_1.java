package com.client.base.uitest.admin;

import com.client.orangehrm.utils.DriverManager;
import com.client.orangehrm.utils.ExcelUtil;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.client.config.Config;
import com.client.orangehrm.libraries.OrangeHRM_Library;

public class Infinity_Learn_1 extends DriverManager {
	
	protected static String className;
	protected static HashMap<Integer, HashMap<String, String>> testData;
	
	protected OrangeHRM_Library OrangeHRMLibrary;

	@BeforeMethod
	public void methodsetup() {
		OrangeHRMLibrary = new OrangeHRM_Library();
	}
	
	
	@Test
	public void MavenDemo() throws Exception {
		String sResult = null;
	
//		Login to Orange HRM Page
		sResult = OrangeHRMLibrary.LoginInfinityLearn(Config.Infinity_APP_URL);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };

		
	}
	
	public void TearDown() throws Exception {
		throw new Exception("Test can't continue, fail here!");
	}

}

