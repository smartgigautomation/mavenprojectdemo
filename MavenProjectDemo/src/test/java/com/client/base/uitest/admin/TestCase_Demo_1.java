package com.client.base.uitest.admin;

import com.client.orangehrm.utils.DriverManager;
import com.client.orangehrm.utils.ExcelUtil;
import com.qa.client.reporting.ExtentManager;
import com.qa.client.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.client.config.Config;
import com.client.orangehrm.libraries.OrangeHRM_Library;

public class TestCase_Demo_1 extends DriverManager {
	
	protected static String className;
	protected static HashMap<Integer, HashMap<String, String>> testData;
	
	protected OrangeHRM_Library OrangeHRMLibrary;
	private ThreadLocal<String> testName = new ThreadLocal<>();

	@BeforeMethod
	public void methodsetup() {
		OrangeHRMLibrary = new OrangeHRM_Library();
	}
	
	@DataProvider(name="getData", parallel = false)
	public Iterator<Object[]> getTestData() throws IOException {
		className = this.getClass().getSimpleName();
		testData = ExcelUtil.getTestData(className);
		
		ArrayList <Object[]> dataProvider = new ArrayList<Object[]>();
		for (Integer currentKey : testData.keySet()) {
			dataProvider.add(new Object[] { testData.get(currentKey) } );
		}
		return dataProvider.iterator();	
	}
	
	@Test(dataProvider = "getData")
	public void MavenDemo(HashMap<String, String> testdata) throws Exception {
		String sResult = null;
		
//		test = report.startTest(testdata.get("TestCaseName"));
//		Creating a test name
		ExtentTestManager.startTest(testdata.get("TestCaseName"), "");
	
//		Login to Orange HRM Page
		sResult = OrangeHRMLibrary.LoginHRM(Config.APP_URL);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
//		Navigating to Users Module
		sResult = OrangeHRMLibrary.NavigateUserModule();
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
//		Creating Users Details
		sResult = OrangeHRMLibrary.CreateUserDetails(testdata);
		if (sResult.equalsIgnoreCase("False")) {TearDown(); };
		
	}
	
	public void TearDown() throws Exception {
		throw new Exception("Test can't continue, fail here!");
	}
	
	@AfterMethod
	public static void EndMethod() {
		ExtentManager.getReporter().flush();
		ExtentTestManager.endTest();
	}
	
	@AfterSuite
	public static void endSuite() {
		ExtentManager.getReporter().flush();
		ExtentTestManager.endTest();
	}

}
