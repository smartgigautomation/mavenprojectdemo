package com.client.orangehrm.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class DriverManager {
	
	public static ThreadLocal<WebDriver> WEB_DRIVER_THREAD = new InheritableThreadLocal<>();
	public ChromeDriver driver;
	
	@BeforeMethod
	public void setup() throws InterruptedException, MalformedURLException {
		WebDriver driver = null;	
		String path = System.getProperty("user.dir") +"\\chromedriver_win32\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", path);
		driver = new ChromeDriver();
		WEB_DRIVER_THREAD.set(driver);
		Thread.sleep(2000);
		driver.manage().window().maximize();
		waitfordriverload();        
        
	}
	
	public WebDriver getwebdriver() {
		WebDriver driver = WEB_DRIVER_THREAD.get();
		return driver;
	}
	
	public void waitfordriverload() throws InterruptedException {
		boolean Status = false;
		JavascriptExecutor js = (JavascriptExecutor) DriverManager.WEB_DRIVER_THREAD.get();
		for (int i=1; i<10; i++) {
			if (js == null) {
				Thread.sleep(250);
				js = (JavascriptExecutor) DriverManager.WEB_DRIVER_THREAD.get();
				continue;
			} else {
				try {
					while(!(js.executeScript("return document.readyState").equals("complete")))
					{
	//					System.out.println("dom state is" +(js.executeScript("return document.readyState")));
						Thread.sleep(500);
					}
					Status = true;
					if (Status = true) { Thread.sleep(250); break; }
				} catch (Exception e) {
					continue;
				}
			}
		}
	}
	
	public static String Capturefullscreenshot() throws IOException {
		String screenshot2;
		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(DriverManager.WEB_DRIVER_THREAD.get());
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ImageIO.write(screenshot.getImage(), "png", bos);
		byte[] imageBytes = bos.toByteArray();
		screenshot2 = "data:image/png;base64," + Base64.getMimeEncoder().encodeToString(imageBytes);
		bos.close();
		return screenshot2;
	}
	
//	@AfterMethod
//	public void closebrowser() {
//		WEB_DRIVER_THREAD.get().close();
//	}
	

}
