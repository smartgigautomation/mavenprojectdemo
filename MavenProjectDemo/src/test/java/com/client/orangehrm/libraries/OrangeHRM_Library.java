package com.client.orangehrm.libraries;

import java.net.SocketTimeoutException;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.client.config.Config;
import com.client.orangehrm.pageobjects.Orange_HRM_Objects;
import com.client.orangehrm.utils.DriverManager;
import com.client.orangehrm.utils.LoggerUtil;
import com.client.orangehrm.utils.WebInteractUtil;
import com.qa.client.reporting.ExtentTestManager;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class OrangeHRM_Library {
	
	public WebDriver driver = DriverManager.WEB_DRIVER_THREAD.get();
	public Orange_HRM_Objects OrangeHRMPage;
	
	public OrangeHRM_Library() {
		OrangeHRMPage = new Orange_HRM_Objects();
	}
	
	public String LoginHRM(String APP_URL) throws Exception {
		
		String Status = null;
		
		WebInteractUtil.launchWebApp(APP_URL);
		WebInteractUtil.isPresent(OrangeHRMPage.UsernameTxb, 75);
		WebInteractUtil.SendKeys(OrangeHRMPage.UsernameTxb, Config.APP_Username);
		WebInteractUtil.SendKeys(OrangeHRMPage.PasswordTxb, Config.APP_Password);
		WebInteractUtil.Click(OrangeHRMPage.LoginBtn);
		WebInteractUtil.Waittilljquesryupdated();
		
		if (WebInteractUtil.waitForElementToBeVisible(OrangeHRMPage.AdminLnk, 60)) {
			LoggerUtil.printExtentLog("Pass", "Application Login is Successfull");
			Status = "True";
		} else {
			LoggerUtil.printExtentLogWithScreenshot("Fail", "Application Login is not successfull, Please Verify");
			Status = "False";
		}	
		return Status;
	}
	
	public String LoginInfinityLearn(String APP_URL) throws Exception {
		
		String Status = null;
		
		WebInteractUtil.launchWebApp(APP_URL);
//		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(OrangeHRMPage.infinityLoginBtn, 60);
		WebInteractUtil.Click(OrangeHRMPage.infinityLoginBtn);
//		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(OrangeHRMPage.phoneNumbertxb, 60);
		WebInteractUtil.SendKeys(OrangeHRMPage.phoneNumbertxb, "6304892801");
		WebInteractUtil.Click(OrangeHRMPage.NextBtn);
//		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(OrangeHRMPage.loginWithPasswordBtn, 60);
		WebInteractUtil.Click(OrangeHRMPage.loginWithPasswordBtn);
//		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.isPresent(OrangeHRMPage.infinityPasswordTxb, 60);
		WebInteractUtil.SendKeys(OrangeHRMPage.infinityPasswordTxb, "123456");
		WebInteractUtil.Click(OrangeHRMPage.infinityLoginWithPasswordBtn);
//		WebInteractUtil.Waittilljquesryupdated();
//		for (int i = 0; i < 5; i++) { WebInteractUtil.Waittilljquesryupdated(); }
		WebInteractUtil.isPresent(OrangeHRMPage.verifyDheerajLabel, 60);
		
		
		
		
//		WebInteractUtil.isPresent(OrangeHRMPage.firstNameTxb, 60);
//		WebInteractUtil.SendKeys(OrangeHRMPage.firstNameTxb, "Smartgig");
//		WebInteractUtil.SendKeys(OrangeHRMPage.lastNameTxb, "Smartgig");
//		WebInteractUtil.Click(OrangeHRMPage.registerVerifyNumberBtn);
//		WebInteractUtil.Waittilljquesryupdated();
		Status = "True";
		
		return Status;
	}
	
	public String NavigateUserModule() throws Exception {
		
		String Status = null;
		
		WebInteractUtil.Click(OrangeHRMPage.AdminLnk);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.Click(OrangeHRMPage.UserManagementLnk);
		WebInteractUtil.Waittilljquesryupdated();
		WebInteractUtil.Click(OrangeHRMPage.UsersLnk);
		WebInteractUtil.Waittilljquesryupdated();
		
		if (WebInteractUtil.waitForElementToBeVisible(OrangeHRMPage.SystemUsersElem, 60)) {
			LoggerUtil.printExtentLogWithScreenshot("Pass","Navigation to User Module is Successfull");
			Status = "True";
		} else {
			LoggerUtil.printExtentLogWithScreenshot("Fail","Navigation to User Module is not Successfull, Please Verify");
			Status = "False";
		}	
		return Status;
		
	}
	
	public String CreateUserDetails(Map<String, String> testdata) throws Exception {
		
		String Status = null;

		WebInteractUtil.Click(OrangeHRMPage.addBtn);
		WebInteractUtil.Waittilljquesryupdated();
		if (WebInteractUtil.waitForElementToBeVisible(OrangeHRMPage.systemUserTypeLsb, 60)) {
			WebInteractUtil.selectByValue(OrangeHRMPage.systemUserTypeLsb, testdata.get("UserRole"));
			WebInteractUtil.SendKeys(OrangeHRMPage.empNameTxb, testdata.get("EmployeeName"));
			WebInteractUtil.SendKeys(OrangeHRMPage.empUserNameTxb, testdata.get("UserName"));
			WebInteractUtil.selectByValue(OrangeHRMPage.empStatusLsb, testdata.get("UserStatus"));
			String ReturnValue = WebInteractUtil.getWebElementText(OrangeHRMPage.passwordHintElem);
			LoggerUtil.logConsoleMessage(ReturnValue);
			WebInteractUtil.SendKeys(OrangeHRMPage.empPasswordTxb, testdata.get("UserPassword"));
			WebInteractUtil.SendKeys(OrangeHRMPage.empConfirmPasswordTxb, testdata.get("ConfirmPassword"));
			WebInteractUtil.Click(OrangeHRMPage.saveBtn);
			LoggerUtil.printExtentLog("Pass","Employee Details has been added successfully");
			Status = "True";
		} else {
			LoggerUtil.printExtentLogWithScreenshot("Fail","systemUserTypeLsb is not visible, Please Verify");
			Status = "False";
		}	
		return Status;
		
	}

}
