package com.qa.client.reporting;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.testng.annotations.BeforeSuite;
import com.client.orangehrm.utils.DriverManager;
import com.relevantcodes.extentreports.ExtentReports;

//OB: ExtentReports extent instance created here. That instance can be reachable by getReporter() method.

public class ExtentManager extends DriverManager{

  private static ExtentReports extent;
  @BeforeSuite
  public synchronized static ExtentReports getReporter(){
      if(extent == null){
          //Set HTML reporting file location
          String workingDir = System.getProperty("user.dir");
          DateFormat df = new SimpleDateFormat("dd-MM-yyyy-HH_mm_ss");
          //File config=new File();          
          extent = new ExtentReports(workingDir+"\\ExtentReports\\ExtentTest -"+df.format(new Date())+".html", true);
          extent.loadConfig(new File(workingDir+"\\config-report.xml"));
      }
      return extent;
  }
  
  
  
}

